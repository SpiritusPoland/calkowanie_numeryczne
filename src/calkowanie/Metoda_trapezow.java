/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calkowanie;

/**
 *
 * @author spiritus
 */
public class Metoda_trapezow {

//funkcja dla ktorej obliczamy calke
    private static double func(double x) {
        return Function.func(x);
    }

    /**
     * @param args
     */
    double xp, xk, dx, calka;
    int n;

    public void setXp(double xp) {
        this.xp = xp;
    }

    public void setXk(double xk) {
        this.xk = xk;
    }

    public void setCalka(double calka) {
        this.calka = calka;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double count() 
        {
        dx = (xk - xp) / (double) n;
        calka = 0;
        for (int i = 1; i < n; i++)
        {
            calka += func(xp + i * dx);
        }
        calka += (func(xp) + func(xk)) / 2;
        calka *= dx;
            System.out.println("calka:"+calka);
        return calka;

    }

}
