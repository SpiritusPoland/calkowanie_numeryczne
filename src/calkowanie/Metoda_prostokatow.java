/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calkowanie;


/**
 *
 * @author spiritus
 */
public class Metoda_prostokatow {
    
    private double xp, xk, dx, calka;
    int n;
    
    
    private static double func(double x)
    {
        return Function.func(x);
    }
    public void set_xp(double a)
    {
        xp=a;
    }
    public void set_xk(double a)
    {
        xk=a;
    }
    public void set_n(int a)
    {
        n=a;
    }
    
    public double count()
    {
     dx = (xk - xp)/(double)n;
      calka = 0;  
      
      for (int i=1; i<=n; i++)
      {
        calka += func(xp + i * dx);
      }
    calka *= dx;
    return calka;
    }
            
    
 
}

