/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calkowanie;

/**
 *
 * @author spiritus
 */
public class Metoda_Monte_Carlo {
    
double xp, xk, yp, yk, calka;
int n, pointsIn;

    public void setXp(double xp) {
        this.xp = xp;
    }

    public void setXk(double xk) {
        this.xk = xk;
    }

    public void setN(int n) {
        this.n = n;
    }
    
 


//funkcja dla ktorej obliczamy calke
private static double func(double x) {
 return Function.func(x);
}
 
//  1 jeżeli punkt leży nad osią OY i jednocześnie pod wykresem funkcji całkowanej
//  -1 jeżeli punkt leży pod osią OY i jednocześnie nad wykresem funkcji całkowanej
//  0 w przeciwnym razie
private static double funcIn(double x, double y) {
if (( y > 0) && (y <= func(x)))
return 1;
else if (( y > 0) && (y <= func(x)))
return -1;
return 0;
}
 
//random number from a to b
private static double randomPoint(double a, double b) {
return  a + Math.random() * (b-a);
}  
 





public double count()
{
    n*=100;
    yp = 0;
    yk = Math.ceil(Math.max(func(xp), func(xk)));
    pointsIn = 0;
 
for (int i=0; i<n; i++) {
pointsIn += funcIn(randomPoint(xp, xk), randomPoint(yp, yk));
}
 
calka = (pointsIn / (double)n) * ((xk-xp) * (yk-yp)); 
System.out.print("Calka: "+calka);
return calka;

}
 
}
